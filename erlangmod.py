
import math
import numpy as np
import scipy.stats as stat
import matplotlib.pyplot as plt


"""
    accepts: 1. offered traffic in Erlangs (a)
             2. number of voice channels in network link (n) - default is 8 (one GSM-900 200kHz band)
    returns: probability that a call is blocked at its attempt time during the busy hour (Grade of Service)
"""


def pr_blocked(a, n=8):
    numerator = (a ** n) / math.factorial(n)
    denominator: float = 0.0

    for i in range(n + 1):
        denominator += (a ** i) / math.factorial(i)

    return numerator / denominator


"""
    accepts: 1. call rate (number of attempted calls in busy hour)
             2. number of voice channels in link (n)
             3. boolean to specify which of two distributions to use for hold times (default: True = 1st distrib, False = 2nd distrib)
    returns: 1. probability that a call is blocked at its attempt time during the busy hour (gos)
             2. offered traffic value for the busy hour (a)
        - lambda_val call start times are obtained from a discrete uniform distribution
        - lambda_val call hold times are obtained from a discrete geometric distribution

"""


def one_simulation(lambda_val, n, first=True):
    # reinitialised for each lambda value (new simulation)
    calls_accepted = 0
    start_times = np.sort(stat.randint.rvs(0, 60, size=lambda_val))  # discrete sorted random uniform variables
    hold_times_geom = np.random.geometric(p=0.5, size=lambda_val)  # discrete random geometric variables (first = True)
    hold_times_gam = stat.gamma.rvs(3, size=lambda_val) # continuous random gamma variables (first = False)
    # convert float values of hold_times_gam to ints
    for i in range(len(hold_times_gam)):
        hold_times_gam[i] = int(round(hold_times_gam[i]))

    # choosing which hold times distribution to use
    hold_times = hold_times_geom if first else hold_times_gam

    link_load = [0] * 60  # list of 60 values initialised to 0s

    for request_index in range(len(start_times)):
        request_time = start_times[request_index]
        if link_load[request_time] < n:
            hold_time = hold_times[request_index]
            calls_accepted += 1
            i = request_time
            end = min(59, request_time + hold_time)  # call 'end' time in busy hour
            while i <= end:
                link_load[i] += 1
                i += 1

    #plot_link_load(link_load)
    #plot_call_requests(start_times)

    calls_rejected = lambda_val - calls_accepted
    rejected_calls_percentage = (calls_rejected / lambda_val) * 100
    a = (lambda_val * np.mean(hold_times)) / 60
    gos = pr_blocked(a, n)

    return gos, a, rejected_calls_percentage


"""
    accepts: array of accepted calls at each minute of busy hour
        - plots a bar chart of accepted calls in place at each minute of the busy hour
"""


def plot_link_load(link_load):
    minutes = np.arange(60)  # tuple of 0 -> 59 values, representing minutes in the busy hour
    plt.bar(minutes, link_load, align='center', alpha=0.5)
    plt.xticks(minutes, minutes)
    plt.ylabel('Accepted Calls Stack')
    plt.xlabel('Minutes in Busy Hour')
    plt.title('Number of Calls at Each Minute in Busy Hour')
    plt.axhline(y=8, linewidth=1, color='r')
    plt.show(block=True)


"""
    accepts: unsorted array of the times of call attempts during the busy hour
        - plot bar chart of number of call attempts made at each minute of the busy hour
"""


def plot_call_requests(start_times):
    minutes = np.arange(60)  # tuple of 0 -> 59 values, representing minutes in the busy hour
    hist = [0] * 60
    for idx in range(len(start_times)):
        hist_idx = start_times[idx]
        hist[hist_idx] += 1
    plt.bar(minutes, hist, align='center', alpha=0.5)
    plt.xticks(minutes, minutes)
    plt.ylabel('Call Attempts')
    plt.xlabel('Minutes in Busy Hour')
    plt.title('Number of Call Attempts at Each Minute in Busy Hour')
    plt.axhline(y=8, linewidth=1, color='r')
    plt.show(block=True)

"""
    accepts: 1. array of offered traffic values (a_arr)
             2. array of gos values for related a values (gos_arr)
        - plots bar chart of probability call is blocked for offered traffic values
             
"""


def plot_gos_vs_a(a_arr, gos_arr, *lambda_val):
    plt.plot(a_arr, gos_arr, '-ok', color='black')
    if lambda_val:
        plt.title('Probability Call Blocked for Offered Traffic Values' + ' (λ = ' + str(lambda_val[0]) + ')')
    else:
        plt.title('Probability Call Blocked for Offered Traffic Values')
    plt.xlabel('Offered Traffic (Erlangs)')
    plt.ylabel('Probability Call Blocked')
    plt.axhline(0, lw=0.5, color='black')
    plt.axvline(0, lw=0.5, color='black')
    plt.show()
