from erlangmod import *


""""
A = lambda * h

A = offered traffic in busy hour (Erlangs)
lambda = total number of call attempts in busy hour (call rate)
h      = average call hold time in busy hour (minutes)
"""

N = 8  # constant number of voice channels in link

gos_arr = []
a_arr = []
rejected_calls_perc = []
lambda_values = np.arange(100, 2400, 100)  # tuple of call rates (number of calls attempts in busy hour)
single_figure = False  # true -> plot curves for all lambdas on same figure, false -> one plot per lambda_val


def monte_carlo_simulation(lambda_values):
    for lambda_val in lambda_values:
        # repeat simulation for each lambda_val 10 times
        for i in range(10):
            t = one_simulation(lambda_val, N, True)
            gos_arr.append(t[0])
            a_arr.append(t[1])
            rejected_calls_perc.append(t[2])
        avg_rejected_calls_perc = np.mean(rejected_calls_perc)
        mean_gos = np.mean(gos_arr)
        std_dev_gos = np.std(gos_arr)
        print('For lambda_val = ' + str(lambda_val) + ' Mean GoS = '
                + str(mean_gos) + ' with Std. Dev. = ' + str(std_dev_gos)
                + ' with Avg. Perc. Rejected = ' + str(avg_rejected_calls_perc))
        agos_xy = np.array([a_arr, gos_arr]) # 2D numpy array created (a_arr is unsorted)
        agos_xy = agos_xy[:, agos_xy[0].argsort()]  # sorted 2D numpy array by row 0 (a values)
        plt.plot(agos_xy[0], agos_xy[1], label=f'lambda={lambda_val}') if single_figure else plot_gos_vs_a(agos_xy[0], agos_xy[1], lambda_val)
    if single_figure:
        plt.title('Probability Call Blocked for Offered Traffic Values')
        plt.xlabel('Offered Traffic (Erlangs)')
        plt.ylabel('Probability Call Blocked')
        plt.legend()
        plt.show()


"""
for single simulation, in erlangmod.one_simulation() uncomment the lines:
    #plot_link_load(link_load)
    #plot_call_requests(start_times)
"""
#one_simulation(200, N) # to get link_load and call_requests bar charts

monte_carlo_simulation(lambda_values)
